(ns ^:figwheel-hooks lhl.demo
  (:require
   [goog.dom :as gdom]
   [clojure.string :as string]
   [reagent.core :as r]
   [reagent.ratom :refer-macros [reaction]]))

(println "This text is printed from src/lhl/demo.cljs. Go ahead and edit it and see reloading in action.")

(defn multiply [a b] (* a b))


;; define your app data so that it doesn't get over-written on reload
(defonce app-state (r/atom {:text "Hello world!"}))

(defn subscribe
  [k]
  (reaction (get @app-state k)))

(defn get-app-element
  "This function gets the element with the id 'app'"
  []
  (gdom/getElement "app"))

(defn change-name-view
  [prompt]
  (let [new-name (r/atom "")]
    (fn [prompt]
      [:div
       [:label prompt
        [:input
         {:value @new-name
          :on-change
          (fn [e]
            (reset! new-name
                    (.. e -target -value)))}]]
       (when-not (string/blank? @new-name)
         [:span (str "Hello, " @new-name)])
       [:button
        {:on-click
         (fn [_]
           (swap! app-state
                  assoc :name @new-name)
           (reset! new-name ""))}
        "Set Name"]])))

(defn text-view
  []
  (let [text (subscribe :text)]
    (fn []
      [:h1 @text])))

(defn name-view
  []
  (let [name (subscribe :name)]
    (fn []
      [:div
       (if-let [n @name]
         (str "Name is " n)
         "No name set")])))

(defn name-length-view
  []
  (let [name-length (reaction (count @(subscribe :name)))]
    (fn []
      [:h3 (str "Name is " @name-length " characters long")])))

(defn hello-world []
  [:div.main {:class "my-class"
              :style {:color "red"}}
   [text-view]
   [:h3 "Hello all!"]
   [:button
    {:on-click
     (fn [_] (swap! app-state
                    update :name
                    str "!"))}
    "Click Me"]
   [name-view]
   [name-length-view]
   [change-name-view "Enter your name"]])

(defn mount [el]
  (r/render-component [hello-world] el))

(defn mount-app-element []
  (when-let [el (get-app-element)]
    (mount el)))

;; conditionally start your application based on the presence of an "app" element
;; this is particularly helpful for testing this ns without launching the app
(mount-app-element)

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  (mount-app-element)
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
)
